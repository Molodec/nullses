package com.example.anull

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun incl (v:View)
    {
        startActivity(Intent(this, Main::class.java))
    }
    fun regcl (v:View)
    {
        startActivity(Intent(this, Register::class.java))
    }
}